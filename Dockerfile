FROM node:13-alpine as builder
WORKDIR doc
ADD *.json ./
COPY . .
RUN yarn && yarn doc

FROM httpd
COPY /home /usr/local/apache2/htdocs/
COPY --from=builder /doc /usr/local/apache2/htdocs/api
EXPOSE 80
