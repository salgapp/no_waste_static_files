project = "no_waste_static_files"

app "nowastedocument" {
  labels = {
    "app" = "homepage",
    "env"     = "dev"
  }

  build {
    use "docker" {}
    registry {
      use "docker" {
        image = "mostela/mussa_homepage"
        tag   = "latest"
      }
    }
  }

  deploy {
    use "kubernetes" {
//      labels = {
//        app: "homepage"
//      }
      ports = [
        {name: "homepage-port", port: 80}
      ]
      namespace = "prod"
    }
  }

  release {
    use "kubernetes" {
      load_balancer = true
      namespace = "prod"
      ports = [
        {
          port: 80, targetPort: "homepage-port"
        }
      ]
    }
  }
}
